﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class LastTask : MonoBehaviour
    {
        private int[] health;
        private bool[] aliveBool = new bool [2];
        private string[] names = new string [3];

        void Start()
        {
            Init();
        }

        void Init()
        {
            health = new int[3];
            health[0] = 10;
            health[1] = 20;
            health[2] = 30;

            for (int i = 0; i < health.Length; i++)
            {
                Debug.Log(health[i]);
            }

            for (int i = 0; i < aliveBool.Length; i++)
            {
                aliveBool[i] = true;
                Debug.Log(aliveBool[i]);
            }

            for (int i = 0; i < names.Length; i++)
            {
                names[0] = "No Name";
                names[1] = "One Name";
                names[2] = "Two Name";
                Debug.Log(names[i]);
            }
        }

    }
}