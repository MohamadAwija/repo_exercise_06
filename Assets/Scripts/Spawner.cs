﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Spawner : MonoBehaviour
    {
        public int spawnCounter;

        void Start()
        {
            FirstSpawner();
            SecondSpawner(0);
            ThirdSpawner(0);
        }

        void FirstSpawner()
        {
            for (int i = 0; i < spawnCounter; i++)
            {
                GameObject newGO = new GameObject();
                Debug.Log("Object Spawned.");
            }
        }

        void SecondSpawner(int i)
        {
            while (i <= spawnCounter)
            {
                GameObject newGO = new GameObject();
                Debug.Log("Object Spawned..");
                i++;
            }
        }

        void ThirdSpawner(int i)
        {
            do
            {
                GameObject newGO = new GameObject();
                Debug.Log("Object Spawned...");
                i++;
            } while (i <= spawnCounter);
        }
    }
}