﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class NumberChecker : MonoBehaviour
    {
        public int[] MyPersonalArray;
        public int numberToSkip = 10;
        public int numberToBreakout = 30;

        void Start()
        {
            
            SecondVoid(20);
        }

        void FirstVoid()
        {
            for (int i = 0; i < MyPersonalArray.Length; i++)
            {
                Debug.Log(MyPersonalArray[i]);
                if (i == 4)
                {
                    Debug.Log("Successful Method");
                }
            }
        }

        void SecondVoid(int Skip)
        {
            for (int i = 0; i < MyPersonalArray.Length; i++)
            {
                if (MyPersonalArray[i] == Skip)
                {
                    Debug.Log("Skip one");
                }
                else
                {
                    Debug.Log(MyPersonalArray[i]);
                }
                if (i == 4)
                {
                    Debug.Log("Done");
                }
            }
        }

        void ThirdVoid(int FirstSkip, int SecondSkip)
        {
            for (int i = 0; i < MyPersonalArray.Length; i++)
            {
                if (MyPersonalArray[i] == FirstSkip)
                {
                    Debug.Log("Skipping.");
                }
                if (MyPersonalArray[i] == SecondSkip)
                {
                    Debug.Log("Breaking out.");
                    break;
                }
                else
                {
                    Debug.Log(MyPersonalArray[i]);
                }
                if (i == 4)
                {
                    Debug.Log("Method is Successful");
                }
            }
        }
    }
}